#!/bin/bash
#$ -S /bin/bash
#$ -N krakensh
#$ -cwd
#$ -o outkrakenSoftTrim.out
#$ -e errkrakenSoftTrim.err
#$ -q short.q
#$ -l h_rt=47:20:00
#$ -pe thread 3
#$ -l h_vmem=35G
#$ -M your@email.com

echo "JOB NAME: $JOB_NAME"
echo "JOB ID: $JOB_ID"
echo "QUEUE: $QUEUE"
echo "HOSTNAME: $HOSTNAME"
echo "SGE O WORKDIR: $SGE_O_WORKDIR"
echo "SGE TASK ID: $SGE_TASK_ID"
echo "NSLOTS: $NSLOTS"

#qsub launchKraken.sh {folder} {single/paired}

module load kraken/2.0.7
myfolder=$1
paired=$2
echo "kraken sur $myfolder"
if [ "$paired" == "paired" ];
then
  for file in $myfolder/*R1_001.fastq.gz
  do
    f=$(basename "$file" R1_001.fastq.gz)
    fr2="$f""R2_001.fastq.gz"
    echo "$f"
    echo "$fr2"
    time kraken2 --db /data2/fdb/kraken/ --gzip-compressed --threads 3 --paired --report $myfolder/$f.report.txt --classified-out $myfolder/$f.clseqs#.fastq --unclassified-out $myfolder/$f.unclseqs#.fq --output $myfolder/$f.output.txt $file $myfolder/$fr2
    unclassifiedReads=$(head -2 $myfolder/$f.report.txt | cut -f2 | head -1)
    classifiedReads=$(head -2 $myfolder/$f.report.txt | cut -f2 | tail -1)
    humanReads=$(grep 'Homo sapiens' $myfolder/$f.report.txt | cut -f2 | head -1)
    bacterialReads=$(grep 'Bacteria' $myfolder/$f.report.txt | cut -f2 | head -1)
    viralReads=$(grep 'Viruses' $myfolder/$f.report.txt | cut -f2 | head -1)
    infofile=$(ls $myfolder/$f*info.txt)
    echo $(($unclassifiedReads * 2 + $classifiedReads * 2)) >> ${infofile}
    echo $(($classifiedReads * 2)) >> ${infofile}
    echo $(($humanReads * 2)) >> ${infofile}
    echo $(($bacterialReads * 2)) >> ${infofile}
    echo $(($viralReads * 2)) >> ${infofile}
  done
elif [ "$paired" == "single" ];
then
  for file in $myfolder/*trimmed.fq.gz
  do
    f=$(basename "$file" *trimmed.fq.gz)
    echo "$f"
    time kraken2 --db /data2/fdb/kraken/ --gzip-compressed --threads 3 --report $myfolder/$f.report.txt --classified-out $myfolder/$f.clseqs_1.fastq --unclassified-out $myfolder/$f.unclseqs_1.fq --output $myfolder/$f.output.txt $file
    unclassifiedReads=$(head -2 $myfolder/$f.report.txt | cut -f2 | head -1)
    classifiedReads=$(head -2 $myfolder/$f.report.txt | cut -f2 | tail -1)
    humanReads=$(grep 'Homo sapiens' $myfolder/$f.report.txt | cut -f2 | head -1)
    bacterialReads=$(grep 'Bacteria' $myfolder/$f.report.txt | cut -f2 | head -1)
    viralReads=$(grep 'Viruses' $myfolder/$f.report.txt | cut -f2 | head -1)
    infofile=$(ls $myfolder/$f*info.txt)
    echo $(($unclassifiedReads + $classifiedReads)) >> ${infofile}
    echo $(($classifiedReads)) >> ${infofile}
    echo $(($humanReads)) >> ${infofile}
    echo $(($bacterialReads)) >> ${infofile}
    echo $(($viralReads)) >> ${infofile}
  done
else
  echo "Missing argument (paired/single)"
fi
