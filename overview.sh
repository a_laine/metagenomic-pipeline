#### Just to create a CSV file to overview samples

#!/bin/bash
ADNfile=$(ls /data1/scratch/masalm/Shapes_Vir/04_Kraken/Viruses/*RNA-.count*)
Infofile=$(ls /data1/scratch/masalm/Azimut/03_Kraken_ARN/*info.txt)
for file in $ADNfile
do
  cat $file | awk -F"," '{print $2}' >> list_species_ADN.txt
done
sort list_species_ADN.txt | uniq >> list_species_ADN_uniq.txt

# for file in $ADNfile
# do
#   while read line; do
#     searchLine=$(grep "$line" "$file")
#     if [ -z "$searchLine" ];
#     then
#       echo "0" >> $file"_table"
#     else
#       echo $searchLine | awk -F"," '{print $3}' >> $file"_table"
#     fi
#   done < list_species_ADN_uniq.txt
# done
#
# paste -d"," /data1/scratch/masalm/Azimut/03_Kraken_ARN/Viruses/*txt_table > table.txt
# paste -d"," list_species_ADN_uniq.txt table.txt > tablefinal.txt
#
# for file in $Infofile
# do
#   cat $file |awk 'NR==3'|tr '\n' ','
# done
