#!/bin/bash
folder=$1

qsub launchPreprocess.sh ${folder}
qsub -hold_jid PreprocessKraken launchKraken.sh ${folder} paired
qsub -hold_jid krakensh launchV3GetIntFasta.sh ${folder} Viruses
qsub -hold_jid krakensh launchV3GetIntFasta.sh ${folder} Bacteria
qsub -hold_jid GetIntFasta launchBlast.sh ${folder}
qsub -hold_jid BlastOnlyVir launchAnalysis.sh ${folder}
