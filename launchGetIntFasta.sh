#!/bin/bash
#$ -S /bin/bash
#$ -N GetIntFasta
#$ -cwd
#$ -o outGetIntFasta.out
#$ -e errGetIntFasta.err
#$ -q short.q
#$ -l h_rt=47:20:00
#$ -pe thread 1
#$ -l h_vmem=75G
#$ -M your@email.com

echo "JOB NAME: $JOB_NAME"
echo "JOB ID: $JOB_ID"
echo "QUEUE: $QUEUE"
echo "HOSTNAME: $HOSTNAME"
echo "SGE O WORKDIR: $SGE_O_WORKDIR"
echo "SGE TASK ID: $SGE_TASK_ID"
echo "NSLOTS: $NSLOTS"

# echo ""
# echo $PATH
# echo $SHELL
# echo "$(pwd)"
# echo ""

source activate EnvAntL
folderInput=../01_Fastq_Test
report=$(ls $folderInput | grep -i .report.)
for reportFile in ${report};
do
  ./GetIntFasta.py $folderInput ${reportFile}
done
source deactivate
