#!/bin/sh
# properties = {"type": "single", "rule": "kraken2", "local": false, "input": ["../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_R1_paired.fq.gz", "../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_R2_paired.fq.gz"], "output": ["../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_.report.txt", "../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_.output.txt", "../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_.clseqs1.fastq", "../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_.clseqs2.fastq"], "wildcards": {"sample": "../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_"}, "params": {}, "log": [], "threads": 3, "resources": {}, "jobid": 10, "cluster": {}}
cd /data2/home/masalm/Antoine/PythonTest && \
/data2/home/masalm/miniconda3/envs/EnvAntL/bin/python3.7 \
-m snakemake ../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_.report.txt --snakefile /data2/home/masalm/Antoine/PythonTest/Snakefile \
--force -j --keep-target-files --keep-remote \
--wait-for-files /data2/home/masalm/Antoine/PythonTest/.snakemake/tmp.1huraruo ../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_R1_paired.fq.gz ../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_R2_paired.fq.gz --latency-wait 5 \
 --attempt 1 --force-use-threads \
--wrapper-prefix https://bitbucket.org/snakemake/snakemake-wrappers/raw/ \
   --allowed-rules kraken2 --nocolor --notemp --no-hooks --nolock \
--mode 2  && touch "/data2/home/masalm/Antoine/PythonTest/.snakemake/tmp.1huraruo/10.jobfinished" || (touch "/data2/home/masalm/Antoine/PythonTest/.snakemake/tmp.1huraruo/10.jobfailed"; exit 1)

