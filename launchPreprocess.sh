#!/bin/bash
#$ -N PreprocessKraken
#$ -cwd
#$ -o outPp.out
#$ -e errPp.err
#$ -q short.q
#$ -l h_rt=47:20:00
#$ -pe thread 10
#$ -l h_vmem=11G
#$ -M your@email.com

echo "JOB NAME: $JOB_NAME"
echo "JOB ID: $JOB_ID"
echo "QUEUE: $QUEUE"
echo "HOSTNAME: $HOSTNAME"
echo "SGE O WORKDIR: $SGE_O_WORKDIR"
echo "SGE TASK ID: $SGE_TASK_ID"
echo "NSLOTS: $NSLOTS"

#qsub launchPreprocess.sh {folder}

source activate EnvAntL
folderInput=$1
cd ${folderInput}
R1fastQgz=$(ls | grep -i R1.*\.fastq\.gz)
for R1fastQgzFile in ${R1fastQgz};
do
  R2fastQgzFile=$(echo ${R1fastQgzFile} | sed 's/R1/R2/')
  dedupe1=$(echo ${R1fastQgzFile} | sed 's/R1/R1_dedupe/')
  dedupe2=$(echo ${R1fastQgzFile} | sed 's/R1/R2_dedupe/')
  if [ -f "${R2fastQgzFile}" ];
  then
    countReads=$(zcat ${R1fastQgzFile} | grep '^+$' | wc -l )
    echo $(($countReads * 2)) > ${R1fastQgzFile%%.*}.info.txt
    echo "PairedEnd Sample"
    clumpify.sh qin=33 in1=${R1fastQgzFile} in2=${R2fastQgzFile} out1=${dedupe1} out2=${dedupe2} dedupe
    echo "Deduplicated"
    trimmomatic PE -threads 10 ${dedupe1} ${dedupe2} ${R1fastQgzFile%%.*}_paired.fq.gz ${R1fastQgzFile%%.*}_unpaired.fq.gz ${R2fastQgzFile%%.*}_paired.fq.gz ${R2fastQgzFile%%.*}_unpaired.fq.gz AVGQUAL:20 MINLEN:50
    echo "Trimmed"
    rm ${dedupe1} ${dedupe2}
  else
    zcat ${R1fastQgzFile} | grep '^+$' | wc -l > ${R1fastQgzFile%%.*}.info.txt
    echo "SingleEnd Sample"
    clumpify.sh qin=33 in=${R1fastQgzFile} out=${dedupe1} dedupe
    echo "Deduplicated"
    trimmomatic SE -threads 10 ${dedupe1} ${R1fastQgzFile%%.*}_trimmed.fq.gz AVGQUAL:20 MINLEN:50
    echo "Trimmed"
    rm ${dedupe1}
  fi
done
source deactivate
