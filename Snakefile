SAMPLES= ["../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job3_","../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job4_","../01_Fastq_Test/RawFastQ/NewFiles/M3S3/Job5_"]


rule all:
    input:
        expand("{sample}R1_paired.fq.gz", sample = SAMPLES),
        expand("{sample}R2_paired.fq.gz", sample = SAMPLES),
        expand("{sample}.info.txt", sample = SAMPLES),

### Remove duplicate reads ###
rule dedupe:
    input:
        raw1 = "{sample}R1.fastq.gz",
        raw2 = "{sample}R2.fastq.gz"
    output:
        dedupe1 = "{sample}R1_dedupe.fastq.gz",
        dedupe2 = "{sample}R2_dedupe.fastq.gz",
    shell:
        """
        clumpify.sh qin=33 in1={input.raw1} in2={input.raw2} out1={output.dedupe1} out2={output.dedupe2} dedupe
        """

### Remove low quality and too short reads (50nt & 20avg quality) ###
rule trimmomatic:
    input:
        dedupe1 = "{sample}R1_dedupe.fastq.gz",
        dedupe2 = "{sample}R2_dedupe.fastq.gz"
    output:
        paired1 = "{sample}R1_paired.fq.gz",
        paired2 = "{sample}R2_paired.fq.gz",
        unpaired1 = "{sample}R1_unpaired.fq.gz",
        unpaired2 = "{sample}R2_unpaired.fq.gz"
    threads: 10
    shell:
        """
        trimmomatic PE -threads 10 {input.dedupe1} {input.dedupe2} {output.paired1} {output.unpaired1} {output.paired2} {output.unpaired2} AVGQUAL:20 MINLEN:50
        rm {input.dedupe1} {input.dedupe2}
        """

### Run kraken2 on each sample ###
rule kraken2:
    input:
        paired1 = "{sample}R1_paired.fq.gz",
        paired2 = "{sample}R2_paired.fq.gz"
    output:
        reportK = "{sample}.report.txt",
        outputK = "{sample}.output.txt",
        clseqs1 = "{sample}.clseqs_1.fastq",
        clseqs2 = "{sample}.clseqs_2.fastq"
    params: cluster="-pe thread 3 -l h_vmem=35G -q short.q -o outkraken.out -e errkraken.err -cwd"
    shell:
        """
        module load kraken/2.0.7
        kraken2 --db /data2/fdb/kraken/ --gzip-compressed --threads 3 --paired --report {wildcards.sample}.report.txt --classified-out {wildcards.sample}.clseqs#.fastq --unclassified-out {wildcards.sample}.unclseqs#.fq --output {wildcards.sample}.output.txt {input.paired1} {input.paired2}
        """

### Creates and fills the info file ###
rule infofile:
    input:
        raw1 = "{sample}R1.fastq.gz",
        reportK = "{sample}.report.txt"
    output:
        info = "{sample}.info.txt"
    shell:
        """
        countReads=$(zcat {input.raw1} | grep '^+$' | wc -l )
        unclassifiedReads=$(head -2 {input.reportK} | cut -f2 | head -1)
        classifiedReads=$(head -2 {input.reportK} | cut -f2 | tail -1)
        humanReads=$(grep 'Homo sapiens' {input.reportK} | cut -f2 | head -1)
        bacterialReads=$(grep 'Bacteria' {input.reportK} | cut -f2 | head -1)
        viralReads=$(grep 'Viruses' {input.reportK} | cut -f2 | head -1)
        echo $(($countReads * 2)) > {output.info}
        echo $(($unclassifiedReads * 2 + $classifiedReads * 2)) >> {output.info}
        echo $(($classifiedReads * 2)) >> {output.info}
        if [ -z "$humanReads" ];
        then
          echo "0" >> {output.info}
        else
          echo $(($humanReads * 2)) >> {output.info}
        fi
        if [ -z "$bacterialReads" ];
        then
          echo "0" >> {output.info}
        else
          echo $(($bacterialReads * 2)) >> {output.info}
        fi
        if [ -z "$viralReads" ];
        then
          echo "0" >> {output.info}
        else
          echo $(($viralReads * 2)) >> {output.info}
        fi
        done
        """
#
# ############## CHANGE INTERESTING FILE DIRECTORY ################
#
# ### Get Viral read names, and recovers the corresponding read ###
# rule getReadsNamesVirus:
#     input:
#         reportK = "{sample}.report.txt"
#     output:
#         "{sample}.report.txtReadsList.txt"
#     shell:
#         """
#         nameFile=$(basename {input.reportK})
#         pathDirectory=$(dirname {input.reportK})
#         mkdir -p ${pathDirectory}/Viruses
#         ./GetIntFasta3.py ${pathDirectory} ${nameFile} Viruses
#         """
#
# rule getReadsVirus:
#     input:
#         readslist = "{sample}.report.txtReadsList.txt",
#         clseqs1 = "{sample}.clseqs_1.fastq",
#         clseqs2 = "{sample}.clseqs_2.fastq"
#     output:
#         "{sample}.interesting.fasta"
#     shell:
#         """
#         ../AlxScripts/RecoverReads.sh {readslist} {clseqs1} {clseqs2} {output}
#         """
#
# ### Get Bacterial read names, and recovers the corresponding read ###
# rule getReadsNamesBacteria:
#     input:
#         reportK = "{sample}.report.txt"
#     output:
#         "{sample}.report.txtReadsList.txt"
#     shell:
#         """
#         nameFile=$(basename {input.reportK})
#         pathDirectory=$(dirname {input.reportK})
#         mkdir -p ${pathDirectory}/Bacteria
#         ./GetIntFasta3.py ${pathDirectory} ${nameFile} Bacteria
#         """
#
# rule getReadsBacteria:
#     input:
#         readslist = "{sample}.report.txtReadsList.txt",
#         clseqs1 = "{sample}.clseqs_1.fastq",
#         clseqs2 = "{sample}.clseqs_2.fastq"
#     output:
#         "{sample}.interesting.fasta"
#     shell:
#         """
#         ../AlxScripts/RecoverReads.sh {readslist} {clseqs1} {clseqs2} {output}
#         """
