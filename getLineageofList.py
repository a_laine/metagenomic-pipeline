# Input: List of species names (txt file)
# Output: Lineages of those species (csv file) (MAY NEED SOME FILE MANAGMENT)
import re
import os
import math
import sys

from ete3 import NCBITaxa
ncbi=NCBITaxa()

entFile=entFolder=sys.argv[1]

lineageFile=open("Lineage_Azimut.csv",'w')
with open(entFile) as taxonomizeSpecies:
    ligne=taxonomizeSpecies.readline()
    while ligne:
        specieName=ligne.strip('\n')
        try:
            taxID=ncbi.get_name_translator([specieName])[specieName][0]
            lineage=ncbi.get_lineage(int(taxID))
            lineage2ranks=ncbi.get_rank(lineage)
            ranks2lineage=dict((rank, taxid) for (taxid, rank) in lineage2ranks.items())
            ranksIwant=['superkingdom','order','family','genus','species']
            listofnames=[]
            for rank in ranksIwant:
                try:
                    lineageName=ncbi.get_taxid_translator([ranks2lineage[rank]])
                    OnlyNames=list(lineageName.values())
                    listofnames.append(OnlyNames)
                except:
                    listofnames.append("unknown")
        except:
            OnlyNames="Unknown lineage"
            listofnames=[OnlyNames]
        lineageFile.write(specieName+','+str(listofnames)+'\n')
        ligne=taxonomizeSpecies.readline()

lineageFile.close()





# cat Lineage_Azimut.csv | tr -d "'" |tr -d "[" |tr -d "]"|tr ";," ",;"|sed 's/; /;/g'
