#!/bin/bash
folder=$1

cd BuildReport
source activate EnvAntL
python run.py ${folder}
source deactivate
mkdir ../resultsPipeline
cp ${folder}/*.html ../resultsPipeline
mkdir ../resultsPipeline/Viruses
mkdir ../resultsPipeline/Bacteria
cp -R ${folder}/Viruses/Depth_Graph* ../resultsPipeline/Viruses
cp -R ${folder}/Viruses/*fasta ../resultsPipeline/Viruses
cp -R ${folder}/Bacteria/*fasta ../resultsPipeline/Bacteria
tar cvf ../resultsPipeline.tar ../resultsPipeline
